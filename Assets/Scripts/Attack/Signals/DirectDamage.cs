﻿using Health;

namespace Attack.Signals
{
    public class DirectDamage
    {
        public AttackComponent From;
        public HealthComponent To;
        public int Damage;
    }
}