using Info;

namespace Attack.Signals
{
    public class Kill
    {
        public InfoComponent Killer;
        public InfoComponent Killed;
        public int Count;
    }
}