using Health;
using Info;

namespace Attack.Signals
{
    public class PlayerDamage
    {
        public HealthComponent Health;
        public int CurrentHealth;
        public int MaxHealth;
    }
}