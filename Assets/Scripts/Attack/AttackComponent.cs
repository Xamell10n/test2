﻿using UnityEngine;
using Weapon;

namespace Attack
{
    public class AttackComponent : MonoBehaviour
    {
        [SerializeField] private WeaponComponent[] _weapons;

        private WeaponComponent _currentWeapon;

        private void Awake()
        {
            _currentWeapon = _weapons[0];
        }

        public void Fire()
        {
            _currentWeapon.Fire(this);
        }
    }
}
