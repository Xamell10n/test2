using ObjectsPool;
using UnityEngine.SceneManagement;

namespace Scene
{
    public class SceneController
    {
        public static void LoadScene(int num)
        {
            ObjectsPoolManager.Clear();
            SceneManager.LoadScene(num);
        }
    }
}