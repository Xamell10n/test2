using System;
using WindowSystem.Interfaces;
using WindowSystem.Managers;

namespace Windows.EndGameWin
{
    public class EndGameWinController : AbstractWindowManager<EndGameWinView>, IOpen<EndGameWinController.Data>
    {
        public EndGameWinController
        (
            EndGameWinView view
            ) : base(view)
        {
            
        }

        public override void Listen()
        {
            
        }
        
        public void Open(Data data)
        {
            var text = string.Format("with {0} kills", data.Kills).ToUpper();
            View.Set(text, data.OnRestart);
        }
        
        public class Data
        {
            public Action OnRestart;
            public int Kills;
        }

    }
}