using Windows.BattleInfo;
using Windows.EndGameLose;
using Windows.EndGameWin;
using Windows.HUD;
using UnityEngine;
using WindowSystem.Installers;
using Zenject;

namespace Windows.Installers
{
    public class GameSceneWindowsInstaller : BaseWindowInstaller
    {
        [SerializeField] private HUDView _hudView;
        [SerializeField] private BattleInfoView _battleInfoView;
        [SerializeField] private EndGameLoseView _endGameLoseView;
        [SerializeField] private EndGameWinView _endGameWinView;
        
        protected override void InstallControllers()
        {
            Container.BindInterfacesAndSelfTo<HUDController>().AsSingle();
            Container.BindInterfacesAndSelfTo<BattleInfoController>().AsSingle();
            Container.BindInterfacesAndSelfTo<EndGameLoseController>().AsSingle();
            Container.BindInterfacesAndSelfTo<EndGameWinController>().AsSingle();
        }

        protected override void InstallViews()
        {
            BindWindow(_hudView);
            BindWindow(_battleInfoView);
            BindWindow(_endGameLoseView);
            BindWindow(_endGameWinView);
        }
    }
}