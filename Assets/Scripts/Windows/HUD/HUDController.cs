using Attack.Signals;
using Health;
using WindowSystem.Interfaces;
using WindowSystem.Managers;
using Zenject;

namespace Windows.HUD
{
    public class HUDController : AbstractWindowManager<HUDView>, IOpen<HUDController.Data>, ILateDisposable, IPopup
    {
        private HealthComponent _currentHealth;
        private SignalBus _signalBus;
        
        public HUDController
        (
            HUDView view,
            SignalBus signalBus
        ) : base(view)
        {
            _signalBus = signalBus;
            
            _signalBus.Subscribe<PlayerDamage>(OnDamage);
        }

        public override void Listen()
        {
            
        }
        
        public void Open(Data data)
        {
            _currentHealth = data.CurrentHealth;
        }
        
        public void LateDispose()
        {
            _signalBus.Unsubscribe<PlayerDamage>(OnDamage);
        }

        private void OnDamage(PlayerDamage data)
        {
            if (data.Health != _currentHealth) return;
            var text = string.Format("HP {0}/{1}", data.CurrentHealth, data.MaxHealth);
            View.Set(text);
        }
        
        public class Data
        {
            public HealthComponent CurrentHealth;
        }
    }
}