using UnityEngine;
using UnityEngine.UI;
using WindowSystem.Views;

namespace Windows.HUD
{
    public class HUDView : AbstractWindowView
    {
        [SerializeField] private Text _health;

        public void Set(string text)
        {
            _health.text = text;
        }
        
        public override void Unlisten()
        {
            
        }
    }
}