using System;
using WindowSystem.Interfaces;
using WindowSystem.Managers;

namespace Windows.EndGameLose
{
    public class EndGameLoseController : AbstractWindowManager<EndGameLoseView>, IOpen<EndGameLoseController.Data>
    {
        public EndGameLoseController
        (
            EndGameLoseView view
            ) : base(view)
        {
            
        }
        
        public void Open(Data data)
        {
            var text = string.Format("your place {0}/{1}", data.Place, data.MaxPlayers);
            View.Set(text, data.OnRestart);
        }

        public override void Listen()
        {
            
        }
        
        public class Data
        {
            public int Place;
            public int MaxPlayers;
            public Action OnRestart;
        }
    }
}