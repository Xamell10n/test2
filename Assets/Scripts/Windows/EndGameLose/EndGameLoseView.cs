using System;
using Plugins.WindowSystem.Elements;
using UnityEngine;
using UnityEngine.UI;
using WindowSystem.Views;

namespace Windows.EndGameLose
{
    public class EndGameLoseView : AbstractWindowView
    {
        [SerializeField] private Text _text;
        [SerializeField] private SimpleButton _restart;

        public void Set(string text, Action onRestart)
        {
            _text.text = text;
            _restart.Listen(onRestart);
        }
        
        public override void Unlisten()
        {
            _restart.Unlisten();
        }
    }
}