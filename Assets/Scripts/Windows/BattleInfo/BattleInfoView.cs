using UnityEngine;
using UnityEngine.UI;
using WindowSystem.Views;

namespace Windows.BattleInfo
{
    public class BattleInfoView : AbstractWindowView
    {
        [SerializeField] private Text[] _messages;

        private int _currentMessage;

        private void Start()
        {
            Reset();
        }

        public void Show(string message)
        {
            for (var i = _messages.Length - 1; i > 0; i--)
            {
                _messages[i].text = _messages[i - 1].text;
            }
            _messages[0].text = message;
        }
        
        public override void Unlisten()
        {
            
        }

        private void Reset()
        {
            foreach (var message in _messages)
            {
                message.text = string.Empty;
            }
        }
    }
}