using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Windows.BattleInfo
{
    public class BattleInfoMessage : MonoBehaviour
    {
        [SerializeField] private Text _text;
        
        public void Set(string message, float delay, Action onHide)
        {
            gameObject.SetActive(true);
            _text.text = message;
            StartCoroutine(DoHide(delay, onHide));
        }

        private IEnumerator DoHide(float delay, Action onHide)
        {
            yield return new WaitForSecondsRealtime(delay);
            Hide();
            onHide();
        }

        private void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}