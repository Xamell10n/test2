using System.Text;
using Attack.Signals;
using WindowSystem.Interfaces;
using WindowSystem.Managers;
using Zenject;

namespace Windows.BattleInfo
{
    public class BattleInfoController : AbstractWindowManager<BattleInfoView>, ILateDisposable, IPopup
    {
        private readonly SignalBus _signalBus;
        
        public BattleInfoController
        (
            BattleInfoView view,
            SignalBus signalBus
        ) : base(view)
        {
            _signalBus = signalBus;
            
            _signalBus.Subscribe<Kill>(OnKill);
        }

        public override void Listen()
        {
            
        }

        public void LateDispose()
        {
            _signalBus.Unsubscribe<Kill>(OnKill);
        }

        private void OnKill(Kill data)
        {
            var text = string.Format("{0} kills ({1}) {2}", data.Killer.Name, data.Count,
                data.Killed.Name);
            View.Show(text);
        }
    }
}