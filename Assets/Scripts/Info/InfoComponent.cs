using UnityEngine;

namespace Info
{
    public class InfoComponent : MonoBehaviour
    {
        [SerializeField] private string _name;
        [SerializeField] private PlayerType _playerType;
        
        public string Name
        {
            get { return _name; }
        }
        
        public PlayerType PlayerType
        {
            get { return _playerType; }
        }

        public void Set(string name, PlayerType playerType)
        {
            _name = name;
            _playerType = playerType;
        }
    }
}