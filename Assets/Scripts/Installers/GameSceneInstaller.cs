using Attack;
using Attack.Signals;
using Damage;
using Inputs.Controllers;
using Managers;
using Spawners;
using UnityEngine;
using Zenject;

public class GameSceneInstaller : MonoInstaller
{
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private StandartInputController _standartInputController;
    [SerializeField] private BotInputController _botInputController;
    
    public override void InstallBindings()
    {
        Container.BindInstance(_playerPrefab);
        Container.Bind<Camera>().FromComponentInHierarchy().AsSingle();
        Container.BindInstance(_standartInputController);
        Container.BindInstance(_botInputController);

        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<DirectDamage>();
        Container.DeclareSignal<Kill>();
        Container.DeclareSignal<PlayerDamage>();

        Container.BindInterfacesAndSelfTo<GameManager>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<LevelLoadManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<SpawnManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerSpawnManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<BotSpawnManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<DamageManager>().AsSingle();
    }
}