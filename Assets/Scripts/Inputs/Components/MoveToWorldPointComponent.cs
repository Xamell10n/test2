﻿using System;
using Constants;
using UnityEngine;
using UnityEngine.AI;

namespace Inputs.Components
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class MoveToWorldPointComponent : BaseMovementComponent
    {
        private NavMeshAgent _navMeshAgent;
        private Action _callback;

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        private void Update()
        {
            if (_callback == null) return;
            var delta = _navMeshAgent.pathEndPosition - transform.position;
            if (delta.sqrMagnitude > 1f) return;
            _callback();
            _callback = null;
        }

        public override void Move(Vector3 pointPosition)
        {
            _navMeshAgent.speed = _speed;

            var ray = new Ray(new Vector3(pointPosition.x, 100f, pointPosition.z), Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, RaycastLayers.Ground))
            {
                var delta = hit.point - transform.position;
                if (delta.sqrMagnitude < 0.01f)
                {
                    return;
                }
                _navMeshAgent.SetDestination(hit.point);
            }
        }

        public void MoveWithCallback(Vector3 pointPosition, Action callback)
        {
            Move(pointPosition);
            _callback = callback;
        }
    }
}