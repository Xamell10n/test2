﻿using UnityEngine;

namespace Inputs.Components
{
    [RequireComponent(typeof(Rigidbody))]
    public class MoveDeltaComponent : BaseMovementComponent
    {
        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public override void Move(Vector3 delta)
        {
            if (delta.sqrMagnitude < 0.01f)
            {
                return;
            }
            _rigidbody.MovePosition(transform.position + delta * _speed * Time.fixedDeltaTime);
        }
    }
}