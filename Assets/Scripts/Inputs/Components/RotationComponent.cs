﻿using UnityEngine;

namespace Inputs.Components
{
    [RequireComponent(typeof(Rigidbody))]
    public class RotationComponent : BaseRotationComponent
    {
        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public override void Rotate(Quaternion quaternion)
        {
            var angle = Quaternion.Lerp(quaternion, quaternion, 0f);
            _rigidbody.MoveRotation(angle);
        }

        public override void Rotate(float delta)
        {
            transform.RotateAround(transform.position, transform.up, delta);
        }
    }
}