﻿using UnityEngine;

namespace Inputs.Components
{
    public abstract class BaseMovementComponent : MonoBehaviour
    {
        protected float _speed;

        public void Set(float speed)
        {
            _speed = speed;
        }
        public abstract void Move(Vector3 pointPosition);
    }
}