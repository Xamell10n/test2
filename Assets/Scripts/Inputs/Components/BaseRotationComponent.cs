﻿using UnityEngine;

namespace Inputs.Components
{
    public abstract class BaseRotationComponent : MonoBehaviour
    {
        public abstract void Rotate(Quaternion quaternion);
        public abstract void Rotate(float delta);
    }
}