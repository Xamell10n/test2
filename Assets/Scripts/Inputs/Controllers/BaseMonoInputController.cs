﻿using Attack;
using Inputs.Components;
using UnityEngine;

namespace Inputs.Controllers
{
    public abstract class BaseMonoInputController : MonoBehaviour
    {
        protected BaseMovementComponent _moveComponent;
        protected BaseRotationComponent _rotationComponent;
        protected AttackComponent _attackComponent;

        public void Set
        (
            BaseMovementComponent moveComponent,
            BaseRotationComponent rotationComponent,
            AttackComponent attackComponent
        )
        {
            _moveComponent = moveComponent;
            _rotationComponent = rotationComponent;
            _attackComponent = attackComponent;
        }

    }
}