﻿using Inputs.Components;
using UnityEngine;
using Zenject;

namespace Inputs.Controllers
{
    public class BotInputController : BaseMonoInputController
    {
        private float _maxShootingPeriod = 5f;
        private float _maxMovementPeriod = 10f;
        private float _maxMovementDistance = 5f;
        private float _rotationSpeed = 15f;

        private bool _isMove;
        private float _nextShootingTime;
        private float _nextMovementTime;
        
        private void Start()
        {
            SetNextShootingTime();
            SetNextMovementTime();
        }
        
        private void Update()
        {
            if (!_attackComponent.gameObject.activeInHierarchy) return;
            if (_nextShootingTime <= Time.time)
            {
                if (_isMove) return;
                _attackComponent.Fire();
                SetNextShootingTime();
            }
        }

        private void FixedUpdate()
        {
            if (!_moveComponent.gameObject.activeInHierarchy) return;
            if (_nextMovementTime <= Time.time)
            {
                var distance = Random.Range(0f, _maxMovementDistance);
                var position = _moveComponent.transform.position +
                               _moveComponent.transform.forward * distance;
                ((MoveToWorldPointComponent)_moveComponent).MoveWithCallback(position, OnEndMovement);
                SetNextMovementTime();
                _isMove = true;
            }
            if (_isMove) return;
            var delta = _rotationSpeed * Time.fixedDeltaTime;
            _rotationComponent.Rotate(delta);
        }

        private void OnEndMovement()
        {
            _isMove = false;
        }

        private void SetNextShootingTime()
        {
            _nextShootingTime = Time.time + Random.Range(1f, _maxShootingPeriod);
        }

        private void SetNextMovementTime()
        {
            _nextMovementTime = Time.time + Random.Range(1f, _maxMovementPeriod);
        }

    }
}