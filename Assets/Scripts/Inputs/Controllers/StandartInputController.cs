﻿using Managers;
using UnityEngine;

namespace Inputs.Controllers
{
    public class StandartInputController : BaseMonoInputController
    {
        private Camera _camera;
        private Quaternion _prevDirection;

        public void Set(Camera camera)
        {
            _camera = camera;
        }

        private void Update()
        {
            if (!_attackComponent.gameObject.activeInHierarchy) return;
            if (!GameManager.IsPlaying) return;
            if (Input.GetAxisRaw(Constants.Axis.Fire1) > 0)
            {
                _attackComponent.Fire();
            }
        }

        private void FixedUpdate()
        {
            if (!_moveComponent.gameObject.activeInHierarchy) return;
            if (!GameManager.IsPlaying) return;
            var xDelta = Input.GetAxis(Constants.Axis.Horizontal);
            var zDelta = Input.GetAxis(Constants.Axis.Vertical);
            var delta = new Vector3(xDelta, 0f, zDelta);
            _moveComponent.Move(delta);
            var angle = MousePositionToAngle();
            _rotationComponent.Rotate(angle);
        }

        private Quaternion MousePositionToAngle()
        {
            var mousePosition = Input.mousePosition;
            var ray = _camera.ScreenPointToRay(mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Constants.RaycastLayers.Ground))
            {
                var direction = hit.point - _rotationComponent.transform.position;
                var rotation = Quaternion.LookRotation(direction);
                rotation = new Quaternion(0f, rotation.y, 0f, rotation.w);
                _prevDirection = rotation;
            }
            return _prevDirection;
        }
    }
}