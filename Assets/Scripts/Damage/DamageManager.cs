using System.Collections.Generic;
using Attack;
using Attack.Signals;
using Health;
using Info;
using ObjectsPool;
using Zenject;

namespace Damage
{
    public class DamageManager : ILateDisposable, IDamageManager
    {
        private readonly SignalBus _signalBus;

        private Dictionary<HealthComponent, int> _healths = new Dictionary<HealthComponent, int>();
        private Dictionary<HealthComponent, int> _maxHealths = new Dictionary<HealthComponent, int>();
        private Dictionary<AttackComponent, int> _kills = new Dictionary<AttackComponent, int>();

        public DamageManager
        (
            SignalBus signalBus
        )
        {
            _signalBus = signalBus;

            _signalBus.Subscribe<DirectDamage>(DirectDamage);
        }

        public void Add(HealthComponent health, AttackComponent attack, int maxHealth)
        {
            _maxHealths.Add(health, maxHealth);
            _healths.Add(health, maxHealth);
            _kills.Add(attack, 0);
        }

        private void DirectDamage(DirectDamage data)
        {
            _healths[data.To] -= data.Damage;
            _signalBus.Fire(new PlayerDamage
            {
                CurrentHealth = _healths[data.To],
                MaxHealth = _maxHealths[data.To],
                Health = data.To
            });
            if (_healths[data.To] <= 0)
            {
                Kill(data.To, data.From);
                return;
            }
            data.To.OnDamage();
        }

        private void Kill(HealthComponent killed, AttackComponent killer)
        {
            killed.OnDie();
            _signalBus.Fire(new Kill
            {
                Killed = killed.GetComponent<InfoComponent>(),
                Killer = killer.GetComponent<InfoComponent>(),
                Count = ++_kills[killer]
            });
            ObjectsPoolManager.Destroy(killed.gameObject);
        }

        public void LateDispose()
        {
            _signalBus.Unsubscribe<DirectDamage>(DirectDamage);
        }
    }
}