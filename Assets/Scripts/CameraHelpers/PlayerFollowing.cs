using Inputs.Components;
using UnityEngine;

namespace CameraHelpers
{
    public class PlayerFollowing : MonoBehaviour
    {
        private BaseMovementComponent _movementComponent;
        private Vector3 _startOffset;

        private void Awake()
        {
            _startOffset = transform.position;
        }

        public void Set(BaseMovementComponent movementComponent)
        {
            _movementComponent = movementComponent;
        }
        
        private void LateUpdate()
        {
            transform.position = _movementComponent.transform.position + _startOffset;
        }
    }
}