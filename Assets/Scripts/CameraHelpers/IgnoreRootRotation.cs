﻿using UnityEngine;

namespace CameraHelpers
{
    public class IgnoreRootRotation : MonoBehaviour
    {
        private Quaternion _startRotation;
        
        private void Start()
        {
            _startRotation = transform.rotation;
        }

        private void LateUpdate()
        {
            transform.rotation = _startRotation;
        }
    }
}