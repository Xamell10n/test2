﻿using UnityEngine;

namespace Spawners
{
    public class SpawnManager
    {
        private readonly PlayerSpawnManager _playerSpawnManager;
        private readonly BotSpawnManager _botSpawnManager;
        
        public SpawnManager
        (
            PlayerSpawnManager playerSpawnManager,
            BotSpawnManager botSpawnManager
        )
        {
            _playerSpawnManager = playerSpawnManager;
            _botSpawnManager = botSpawnManager;
        }
        
        public void Spawn(int maxPlayers)
        {
            _playerSpawnManager.Spawn(Vector3.zero, Quaternion.identity);
            for (var i = 0; i < maxPlayers - 1; i++)
            {
                var position = GetSpawnPoint();
                var rotation = Random.Range(0f, 1f);
                var quaternion = new Quaternion(0f, 1f, 0f, rotation);
                _botSpawnManager.Spawn(position, quaternion);
            }
        }

        private Vector3 GetSpawnPoint()
        {
            Vector3 point;
            while (true)
            {
                var xPoint = GetRandomCoordinate();
                var zPoint = GetRandomCoordinate();
                point = new Vector3(xPoint, 5f, zPoint);
                if (Physics.Raycast(point, Vector3.down, Constants.RaycastLayers.Ground))
                {
                    break;
                }
            }
            return point;
        }

        private float GetRandomCoordinate()
        {
            return Random.Range(-45f, 45f);
        }
    }
}