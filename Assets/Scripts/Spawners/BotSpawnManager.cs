﻿using System.Collections.Generic;
using Attack;
using Damage;
using Health;
using Info;
using Inputs.Components;
using Inputs.Controllers;
using ObjectsPool;
using UnityEngine;
using Zenject;

namespace Spawners
{
    public class BotSpawnManager
    {
        private float _enemySpeed = 3f;
        private int _maxEnemyHealth = 3;
        private int _counter;

        private readonly GameObject _playerPrefab;
        private readonly DiContainer _diContainer;
        private readonly DamageManager _damageManager;

        public BotSpawnManager
        (
            GameObject playerPrefab,
            DamageManager damageManager,
            DiContainer diContainer
        )
        {
            _playerPrefab = playerPrefab;
            _diContainer = diContainer;
            _damageManager = damageManager;
        }

        public void Spawn(Vector3 position, Quaternion rotation)
        {
            var enemy = ObjectsPoolManager.Instantiate(_playerPrefab, position, rotation,
                Constants.SpawnPlaces.Players);
            var name = "Bot" + ++_counter;
            enemy.AddComponent<InfoComponent>().Set(name, PlayerType.Bot);
            enemy.name = name;
            var movementComponent = enemy.AddComponent<MoveToWorldPointComponent>();
            movementComponent.Set(_enemySpeed);
            var rotationComponent = enemy.AddComponent<RotationComponent>();
            var attackComponent = enemy.GetComponent<AttackComponent>();
            var prefab = _diContainer.Resolve<BotInputController>();
            var inputController = ObjectsPoolManager.Instantiate(prefab, Vector3.zero,
                Quaternion.identity, Constants.SpawnPlaces.Inputs);
            inputController.Set(movementComponent, rotationComponent, attackComponent);
            _damageManager.Add(enemy.GetComponent<HealthComponent>(), attackComponent, _maxEnemyHealth);
            _diContainer.InjectGameObject(enemy);
        }
    }
}