﻿using System.Collections.Generic;
using System.Linq;
using Windows.HUD;
using Attack;
using CameraHelpers;
using Damage;
using Health;
using Info;
using Inputs.Components;
using Inputs.Controllers;
using ObjectsPool;
using UnityEngine;
using WindowSystem.Managers;
using Zenject;

namespace Spawners
{
    public class PlayerSpawnManager
    {
        private float _playerSpeed = 5f;
        private int _maxPlayerHealth = 3;
        private int _counter;

        private readonly GameObject _playerPrefab;
        private readonly Camera _cameraPrefab;
        private readonly DiContainer _diContainer;
        private readonly DamageManager _damageManager;
        private readonly WindowsManager _windowsManager;

        public PlayerSpawnManager
        (
            GameObject playerPrefab,
            DiContainer diContainer,
            DamageManager damageManager,
            WindowsManager windowsManager,
            Camera cameraPrefab
        )
        {
            _playerPrefab = playerPrefab;
            _cameraPrefab = cameraPrefab;
            _diContainer = diContainer;
            _damageManager = damageManager;
            _windowsManager = windowsManager;
        }

        public void Spawn(Vector3 position, Quaternion rotation)
        {
            var player = ObjectsPoolManager.Instantiate(_playerPrefab, position, rotation,
                Constants.SpawnPlaces.Players);
            var name = "Player" + ++_counter;
            player.AddComponent<InfoComponent>().Set(name, PlayerType.Player);
            player.name = name;
            var movementComponent = player.AddComponent<MoveDeltaComponent>();
            movementComponent.Set(_playerSpeed);
//            var camera = GameObject.Instantiate(_cameraPrefab);
            _cameraPrefab.gameObject.AddComponent<PlayerFollowing>().Set(movementComponent);
            var rotationComponent = player.AddComponent<RotationComponent>();
            var attackComponent = player.GetComponent<AttackComponent>();
            var prefab = _diContainer.Resolve<StandartInputController>();
            var inputController = ObjectsPoolManager.Instantiate(prefab, Vector3.zero,
                    Quaternion.identity, Constants.SpawnPlaces.Inputs);
            inputController.Set(movementComponent, rotationComponent, attackComponent);
            inputController.Set(_cameraPrefab);
            var health = player.GetComponent<HealthComponent>();
            _damageManager.Add(health, attackComponent, _maxPlayerHealth);
            var data = new HUDController.Data {CurrentHealth = health};
            _windowsManager.OpenWindow<HUDController, HUDController.Data>(data);
            _diContainer.InjectGameObject(player);
        }
    }
}