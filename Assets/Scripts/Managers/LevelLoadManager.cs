﻿using Spawners;
using Zenject;

namespace Managers
{
    public class LevelLoadManager 
    {
        private readonly SpawnManager _spawner;

        public LevelLoadManager
        (
            SpawnManager spawner
        )
        {
            _spawner = spawner;
        }
        
        public void Load(int maxPlayers)
        {
            _spawner.Spawn(maxPlayers);
        }
    }
}