﻿using Windows.BattleInfo;
using Windows.EndGameLose;
using Windows.EndGameWin;
using Attack.Signals;
using Info;
using Scene;
using WindowSystem.Managers;
using Zenject;

namespace Managers
{
    public class GameManager : IInitializable, ILateDisposable
    {
        public static bool IsPlaying { get; private set; }
        
        private int _maxPlayers = 50;
        private int _currentPlayers;

        private readonly LevelLoadManager _levelLoadManager;
        private readonly SignalBus _signalBus;
        private readonly WindowsManager _windowsManager;

        public GameManager
        (
            LevelLoadManager levelLoadManager,
            WindowsManager windowsManager,
            SignalBus signalBus
        )
        {
            _levelLoadManager = levelLoadManager;
            _signalBus = signalBus;
            _windowsManager = windowsManager;
            
            _signalBus.Subscribe<Kill>(OnKill);
        }

        public void Initialize()
        {
            _levelLoadManager.Load(_maxPlayers);
            _currentPlayers = _maxPlayers;
            _windowsManager.OpenWindow<BattleInfoController>();
            IsPlaying = true;
        }
        
        public void LateDispose()
        {
            _signalBus.Unsubscribe<Kill>(OnKill);
        }

        private void OnKill(Kill data)
        {
            if (data.Killed.PlayerType == PlayerType.Player)
            {
                LoseGame();
                return;
            }
            if (data.Killed.PlayerType == PlayerType.Bot && --_currentPlayers <= 1)
            {
                WinGame(data.Count);
            }
        }

        private void WinGame(int kills)
        {
            var data = new EndGameWinController.Data {OnRestart = OnRestart, Kills = kills};
            _windowsManager.OpenWindow<EndGameWinController, EndGameWinController.Data>(data);
            IsPlaying = false;
        }

        private void LoseGame()
        {
            var data = new EndGameLoseController.Data
                {OnRestart = OnRestart, MaxPlayers = _maxPlayers, Place = _currentPlayers};
            _windowsManager.OpenWindow<EndGameLoseController, EndGameLoseController.Data>(data);
        }

        private void OnRestart()
        {
            SceneController.LoadScene(0);
        }
    }
}