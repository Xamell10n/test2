﻿using Attack;
using Info;
using ObjectsPool;
using UnityEngine;
using Zenject;

namespace Weapon
{
    public class WeaponComponent : MonoBehaviour
    {
        [SerializeField] private GameObject _ammoPrefab;
        [SerializeField] private Transform _firePoint;
        [SerializeField] private float _reloadTime;

        [Inject] private SignalBus _signalBus;
        private float _nextTimeToShoot;

        public void Fire(AttackComponent component)
        {
            if (_nextTimeToShoot <= Time.time)
            {
                ObjectsPoolManager
                    .Instantiate(_ammoPrefab, _firePoint.position, _firePoint.transform.rotation, "Ammo")
                    .GetComponent<Ammo.Ammo>().Set(component, _signalBus);
                _nextTimeToShoot = Time.time + _reloadTime;
            }
        }
    }
}