﻿using Attack;
using UnityEngine;
using Zenject;

namespace Ammo
{
    public abstract class Ammo : MonoBehaviour
    {
        protected SignalBus _signalBus;
        protected AttackComponent _owner;

        public void Set(AttackComponent owner, SignalBus signalBus)
        {
            _owner = owner;
            _signalBus = signalBus;
        }
    }
}