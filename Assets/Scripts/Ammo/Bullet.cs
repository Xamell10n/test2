﻿using Attack.Signals;
using Health;
using ObjectsPool;
using UnityEngine;

namespace Ammo
{
    [RequireComponent(typeof(Rigidbody))]
    public class Bullet : Ammo, IPoolObject
    {
        [SerializeField] private int _damage = 1;
        [SerializeField] private float _speed = 1f;

        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            _rigidbody.MovePosition(transform.position +
                                    transform.forward * _speed * Time.fixedDeltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            var health = other.GetComponent<HealthComponent>();
            if (health)
            {
                _signalBus.Fire(new DirectDamage
                    {Damage = _damage, From = _owner, To = health});
            }
            ObjectsPoolManager.Destroy(this);
        }

        public void Reset()
        {
        }
    }
}