﻿namespace Constants
{
    public class Axis
    {
        public static readonly string Horizontal = "Horizontal";
        public static readonly string Vertical = "Vertical";
        public static readonly string Fire1 = "Fire1";
    }
}