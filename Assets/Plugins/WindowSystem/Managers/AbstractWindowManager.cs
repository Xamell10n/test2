﻿using WindowSystem.Interfaces;

namespace WindowSystem.Managers
{
    public abstract class AbstractWindowManager<T> : IWindowManager where T : IWindowView
    {
        protected readonly T View;

        protected AbstractWindowManager
        (
            T view
        )
        {
            View = view;
        }

        public void Open()
        {
            View.Open();
        }

        public void Close()
        {
            View.Close();
        }

        public abstract void Listen();

        public virtual void Unlisten()
        {
            View.Unlisten();
        }
    }
}