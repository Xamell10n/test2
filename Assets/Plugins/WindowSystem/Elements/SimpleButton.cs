using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Plugins.WindowSystem.Elements
{
    public class SimpleButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        private Action _callback;
        
        public void Listen(Action callback)
        {
            _callback = callback;
        }

        public void Unlisten()
        {
            _callback = null;
        }
        
        public void OnPointerDown(PointerEventData eventData)
        {
            
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_callback != null)
            {
                _callback();
            }
        }
    }
}