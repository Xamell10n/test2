using UnityEngine;
using WindowSystem.Views;
using Zenject;

namespace WindowSystem.Installers
{
    public abstract class BaseWindowInstaller : MonoInstaller
    {
        [Inject] private Canvas _rootCanvas;
        
        public override void InstallBindings()
        {
            InstallControllers();
            InstallViews();
        }

        protected abstract void InstallControllers();
        protected abstract void InstallViews();

        protected void BindWindow<T>(T prefab) where T : AbstractWindowView
        {
            var go = Instantiate(prefab, _rootCanvas.transform, false);
            go.gameObject.SetActive(false);
            Container.BindInstance(go).AsSingle();
        }

    }
}