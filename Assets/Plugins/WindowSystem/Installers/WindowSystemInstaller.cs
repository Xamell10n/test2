﻿using UnityEngine;
using WindowSystem.Managers;
using Zenject;

namespace WindowSystem.Installers
{
    public class WindowSystemInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<WindowsFabric>().AsSingle();
            Container.BindInterfacesAndSelfTo<WindowsManager>().AsSingle();
            Container.Bind<Canvas>().FromComponentInHierarchy().WhenInjectedInto<BaseWindowInstaller>();
        }
    }
}