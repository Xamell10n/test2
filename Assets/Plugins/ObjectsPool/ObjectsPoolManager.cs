﻿using System.Collections.Generic;
using UnityEngine;

namespace ObjectsPool
{
    public class ObjectsPoolManager
    {
        private static readonly Dictionary<GameObject, List<GameObject>> _objects =
            new Dictionary<GameObject, List<GameObject>>();

        private static readonly Dictionary<string, Transform> _transforms =
            new Dictionary<string, Transform>();

        public static GameObject Instantiate(GameObject prefab, Vector3 position,
            Quaternion rotation)
        {
            GameObject gameObject = null;
            if (_objects.ContainsKey(prefab))
            {
                foreach (var @object in _objects[prefab])
                {
                    if (@object.activeInHierarchy) continue;
                    gameObject = @object;
                    gameObject.SetActive(true);
                    gameObject.transform.position = position;
                    gameObject.transform.rotation = rotation;
                    break;
                }
                if (gameObject == null)
                {
                    gameObject = GameObject.Instantiate(prefab, position, rotation);
                    _objects[prefab].Add(gameObject);
                }
            }
            else
            {
                gameObject = GameObject.Instantiate(prefab, position, rotation);
                _objects[prefab] = new List<GameObject> {gameObject};
            }
            return gameObject;
        }

        public static GameObject Instantiate(GameObject prefab, Vector3 position,
            Quaternion rotation, Transform parent)
        {
            var gameObject = Instantiate(prefab, position, rotation);
            gameObject.transform.SetParent(parent);
            return gameObject;
        }

        public static GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation,
            string parentName)
        {
            if (!_transforms.ContainsKey(parentName))
            {
                var parentGameObject = GameObject.Find(parentName);
                Transform parentTransform;
                if (parentGameObject)
                {
                    parentTransform = parentGameObject.transform;
                }
                else
                {
                    parentTransform = new GameObject(parentName).transform;
                }
                _transforms[parentName] = parentTransform;
            }
            return Instantiate(prefab, position, rotation, _transforms[parentName]);
        }

        public static T Instantiate<T>(T prefab, Vector3 position, Quaternion rotation,
            string parentName) where T : MonoBehaviour
        {
            var gameObject = Instantiate(prefab.gameObject, position, rotation, parentName);
            return gameObject.GetComponent<T>();
        }

        public static void Destroy(ObjectInPool @object)
        {
            @object.Reset();
            @object.gameObject.SetActive(false);
        }
        
        public static void Destroy(IPoolObject @object)
        {
            @object.Reset();
            ((MonoBehaviour) @object).gameObject.SetActive(false);
        }
        
        public static void Destroy(GameObject gameObject)
        {
            var objectInPool = gameObject.GetComponent<ObjectInPool>();
            if (objectInPool)
            {
                objectInPool.Reset();
            }
            gameObject.SetActive(false);
        }

        public static void Clear()
        {
            _objects.Clear();
            _transforms.Clear();
        }

        public static void Preload(GameObject prefab, int num, string parent)
        {
            for (var i = 0; i < num; i++)
            {
                Instantiate(prefab, Vector3.zero, Quaternion.identity, parent);
            }
        }
    }
}