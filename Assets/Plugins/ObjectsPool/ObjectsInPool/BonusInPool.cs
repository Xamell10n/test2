﻿using UnityEngine;

namespace ObjectsPool
{
    public class BonusInPool : ObjectInPool
    {
        [SerializeField] private SpriteRenderer _renderer;
        [SerializeField] private Collider _collider;
        
        public override void Reset()
        {
            _renderer.enabled = false;
            _collider.enabled = false;

        }
    }
}