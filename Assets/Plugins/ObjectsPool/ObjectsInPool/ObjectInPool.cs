﻿using UnityEngine;

namespace ObjectsPool
{
    public abstract class ObjectInPool : MonoBehaviour
    {
        public abstract void Reset();
    }
}