﻿namespace ObjectsPool
{
    public interface IPoolObject
    {
        void Reset();
    }
}